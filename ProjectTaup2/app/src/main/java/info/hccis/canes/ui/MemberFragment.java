package info.hccis.canes.ui;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import info.hccis.canes.MemberApplication;
import info.hccis.canes.entity.Member;

import info.hccis.canes.R;
import info.hccis.canes.bo.MemberContentViewModel;

public class MemberFragment extends Fragment {

    private RecyclerView mRecyclerView;
    public MemberFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_member, container, false);

        LinearLayoutManager layoutManager = new LinearLayoutManager(
                getActivity());
        mRecyclerView = (RecyclerView) view
                .findViewById(R.id.member_recycler_view);
        mRecyclerView.setLayoutManager(layoutManager);
        updateUI();

        return view;
    }
    private void updateUI(){
        //List<Member> members = new ArrayList<Member>();
        //members.add(new Member(90,90,90, "71 Kensington rd", 7,"02/02/2019", "02/02/2019"));

        RecyclerView.Adapter adapter = new MemberListAdapter(MemberContentViewModel.members);
        mRecyclerView.setAdapter(adapter);
    }

    public class MemberListAdapter extends RecyclerView.Adapter<MemberListAdapter.MemberViewHolder> {
        private List<Member> members;
        private Context mContext;

        public MemberListAdapter(List<Member> Members) {
            this.members = Members;
        }

        public MemberListAdapter(Context context, List<Member> Members) {
            mContext = context;
            members = Members;
        }

        @Override
        public MemberListAdapter.MemberViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_member, parent, false);
            MemberViewHolder viewHolder = new MemberViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(MemberListAdapter.MemberViewHolder holder, int position) {
            Member Member = members.get(position);
            holder.bind(Member);
        }

        @Override
        public int getItemCount() {
            return members.size();
        }

        public class MemberViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            private Context mContext;
            private Member Member;
            TextView memberTextView;

            public void bind(Member Member) {
                this.Member = Member;
                memberTextView.setText(this.Member.getAddress());
            }

            public MemberViewHolder(View itemView) {
                super(itemView);
                this.memberTextView = itemView.findViewById(R.id.memberID);
                mContext = itemView.getContext();
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {


            }

        }

    }
}