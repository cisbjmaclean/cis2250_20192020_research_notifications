package info.hccis.canes.ui;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import info.hccis.canes.MainActivity;
import info.hccis.canes.MemberApplication;
import info.hccis.canes.R;
import info.hccis.canes.bo.MemberContentViewModel;

public class HomeFragment extends Fragment  implements View.OnClickListener {

    private NotificationManagerCompat notificationManager;
    public HomeFragment(){}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        MemberContentViewModel model = ViewModelProviders.of(this).get(MemberContentViewModel.class);

        Button viewMembers = view.findViewById(R.id.view_members);
        viewMembers.setOnClickListener(this);
        Button notificationButton = view.findViewById(R.id.example_notification_button);
        notificationButton.setOnClickListener(this);
        notificationManager = NotificationManagerCompat.from(getContext());
        return view;
    }
    private synchronized void sendNotification(){
        //Channel Id is ignored on lower APIs
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(MemberApplication.getContext(), MemberApplication.MEMBER_CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_android_black_24dp)
                        .setContentTitle("Notification")
                        .setContentText("message");

        NotificationManager notificationManager = (NotificationManager) MemberApplication.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, builder.build());
    }
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.view_members:
                getFragmentManager().beginTransaction().replace(R.id.fragment_container,new MemberFragment()).addToBackStack(null).commit();
            break;
            case R.id.example_notification_button:
                sendNotification();
        }
    }

}
