package info.hccis.canes.util;

import java.util.List;

import info.hccis.canes.entity.Member;
import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonMemberApi {
    @GET("member")
    Call<List<Member>> getMembers();
}
