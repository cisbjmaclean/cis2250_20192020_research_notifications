package info.hccis.canes.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "member")
public class Member {

    public static final String MEMBER_BASE_API = "http://hccis.info:8080/court/rest/MemberService/";

    @ColumnInfo(name="id")
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name="phoneCell")
    private long phoneCell;

    @ColumnInfo(name="phoneHome")
    private long phoneHome;

    @ColumnInfo(name="phoneWork")
    private long phoneWork;

    @ColumnInfo(name="address")
    private String address;

    @ColumnInfo(name="status")
    private int status;

    @ColumnInfo(name="membershipStartDate")
    private String membershipStartDate;

    @ColumnInfo(name="membershipEndDate")
    private String getmembershipEndDate;

    public Member(){

    }

    public Member(long phoneCell, long phoneHome,  long phoneWork, String address, int status, String membershipStartDate, String getmembershipEndDate) {
        this.phoneCell = phoneCell;
        this.phoneHome = phoneHome;
        this.phoneWork = phoneWork;
        this.address = address;
        this.status = status;
        this.membershipStartDate = membershipStartDate;
        this.getmembershipEndDate = getmembershipEndDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getPhoneCell() {
        return phoneCell;
    }

    public void setPhoneCell(long phoneCell) {
        this.phoneCell = phoneCell;
    }

    public long getPhoneHome() {
        return phoneHome;
    }

    public void setPhoneHome(long phoneHome) {
        this.phoneHome = phoneHome;
    }

    public long getPhoneWork() {
        return phoneWork;
    }

    public void setPhoneWork(int phoneWork) {
        this.phoneWork = phoneWork;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getmembershipStartDate() {
        return membershipStartDate;
    }

    public void setmembershipStartDate(String membershipStartDate) {
        this.membershipStartDate = membershipStartDate;
    }

    public String getGetmembershipEndDate() {
        return getmembershipEndDate;
    }

    public void setGetmembershipEndDate(String getmembershipEndDate) {
        this.getmembershipEndDate = getmembershipEndDate;
    }

    @Override
    public String toString() {
        return "Member{" +
                "id=" + id +
                ", phoneCell=" + phoneCell +
                ", phoneHome=" + phoneHome +
                ", phoneWork=" + phoneWork +
                ", address='" + address + '\'' +
                ", status=" + status +
                ", membershipStartDate='" + membershipStartDate + '\'' +
                ", getmembershipEndDate='" + getmembershipEndDate + '\'' +
                '}';
    }
}
