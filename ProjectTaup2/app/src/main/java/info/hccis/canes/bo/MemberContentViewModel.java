package info.hccis.canes.bo;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.View;

import androidx.core.app.NotificationCompat;
import androidx.lifecycle.ViewModel;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import info.hccis.canes.MainActivity;
import info.hccis.canes.MemberApplication;
import info.hccis.canes.R;
import info.hccis.canes.entity.Member;
import info.hccis.canes.util.JsonMemberApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.provider.Settings.System.getString;

public class MemberContentViewModel  extends ViewModel {

    static public List<Member> members;
    public MemberContentViewModel(){
        loadMembers();
    }


    private void loadMembers() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Member.MEMBER_BASE_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonMemberApi jsonMemberApi = retrofit.create(JsonMemberApi.class);

        Call<List<Member>> call = jsonMemberApi.getMembers();

        call.enqueue(new Callback<List<Member>>() {

            @Override
            public void onResponse(Call<List<Member>> call, Response<List<Member>> response) {
                if (!response.isSuccessful()) {
                    Log.d("bjm", "Code" + response.code());
                    return;
                }
                members = response.body();
            }

            @Override
            public void onFailure(Call<List<Member>> call, Throwable t) {
                Log.e("bjm", t.getMessage());
            }
        });
        sendNotification();

    }
    private synchronized void sendNotification() {
        //Channel Id is ignored on lower APIs
        Log.d("isSent", "sent from member content vieew");
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(MemberApplication.getContext(), MemberApplication.MEMBER_CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_android_black_24dp)
                        .setContentTitle("JSON Notification")
                        .setContentText("Done downloading Members");

        NotificationManager notificationManager = (NotificationManager) MemberApplication.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, builder.build());
    }

}
